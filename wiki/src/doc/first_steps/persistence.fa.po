# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2020-09-11 17:23+0000\n"
"PO-Revision-Date: 2015-10-14 17:02+0000\n"
"Last-Translator: sprint5 <translation5@451f.org>\n"
"Language-Team: Persian <http://weblate.451f.org:8889/projects/tails/"
"persistence/fa/>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 2.4-dev\n"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "[[!meta title=\"Encrypted persistence\"]]\n"
msgid "[[!meta title=\"Encrypted Persistent Storage\"]]\n"
msgstr "[[!meta title=\"مانای رمزگذاری‌شده\"]]\n"

#. type: Plain text
#, fuzzy
#| msgid ""
#| "If you start Tails from a USB stick or SD card, you can create a\n"
#| "persistent volume in the free space left on the device by <span\n"
#| "class=\"application\">Tails Installer</span>. The files in the "
#| "persistent\n"
#| "volume are saved encrypted and remain available across separate working "
#| "sessions.\n"
msgid ""
"If you start Tails from a USB stick, you can create a Persistent Storage in "
"the free space left on the USB stick.  The files and settings stored in the "
"Persistent Storage are saved encrypted and remain available across different "
"working sessions."
msgstr ""
"اگر تیلز را از روی درایو یواس‌بی یا کارت حافظه راه‌اندازی می‌کنید، می‌توانید با "
"استفاده از\n"
"<span class=\"application\">نصب کنندهٔ تیلز</span> یک درایو مانا در فضای خالی "
"روی دستگاه‌تان ایجاد کنید.\n"
"فایل‌های روی درایو مانا به صورت رمزگذاری شده ذخیره می‌شوند و برای نشست‌های کاری "
"جداگانه قابل استفاده‌اند.\n"

#. type: Plain text
#, fuzzy
#| msgid ""
#| "You can use this persistent volume to store different kinds of files:"
msgid "You can use this Persistent Storage to store, for example:"
msgstr "با استفاده از این درایو مانا می‌توانید فایل‌های مختلفی را نگهداری کنید:"

#. type: Bullet: '  - '
msgid "Personal files"
msgstr ""

#. type: Bullet: '  - '
msgid "Some settings"
msgstr ""

#. type: Bullet: '  - '
msgid "Additional software"
msgstr ""

#. type: Bullet: '  - '
#, fuzzy
#| msgid "your encryption keys"
msgid "Encryption keys"
msgstr "کلیدهای رمزگذاری شما"

#. type: Plain text
#, fuzzy
#| msgid ""
#| "The persistent volume is an encrypted partition protected by a passphrase."
msgid ""
"The Persistent Storage is an encrypted partition protected by a passphrase "
"on the USB stick."
msgstr "درایو مانا یک درایو رمزگذاری شده است که با یک گذرواژه محافظت می‌شود."

#. type: Plain text
#, fuzzy
#| msgid ""
#| "Once the persistent volume is created, you can choose to activate it or "
#| "not each time you start Tails."
msgid ""
"After you create a Persistent Storage, you can choose to unlock it or not "
"each time you start Tails."
msgstr ""
"وقتی درایو مانا ساخته می‌شود، هر بار که تیلز را راه‌اندازی می‌کنید می‌توانید "
"انتخاب کنید که فعال شود یا نه."

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/first_steps/persistence.caution\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"doc/first_steps/persistence.caution.fa\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Title =
#, fuzzy, no-wrap
#| msgid "You can use this persistent volume to store different kinds of files:"
msgid "How to use the Persistent Storage"
msgstr "با استفاده از این درایو مانا می‌توانید فایل‌های مختلفی را نگهداری کنید:"

#. type: Bullet: '  - '
#, fuzzy
#| msgid "[[Warnings about persistence|first_steps/persistence/warnings]]"
msgid ""
"[[Warnings about the Persistent Storage|first_steps/persistence/warnings]]"
msgstr "[[هشدارهایی درباره‌ی مانا|first_steps/persistence/warnings]]"

#. type: Bullet: '  - '
#, fuzzy
#| msgid ""
#| "[[Create & configure the persistent volume|first_steps/persistence/"
#| "configure]]"
msgid ""
"[[Creating and configuring the Persistent Storage|first_steps/persistence/"
"configure]]"
msgstr "[[ساخت و تنظیم درایو مانا|first_steps/persistence/configure]]"

#. type: Bullet: '  - '
#, fuzzy
#| msgid "[[Enable & use the persistent volume|first_steps/persistence/use]]"
msgid ""
"[[Unlocking and using the Persistent Storage|first_steps/persistence/use]]"
msgstr "[[فعال کردن و استفاده از درایو مانا|first_steps/persistence/use]]"

#. type: Bullet: '  - '
#, fuzzy
#| msgid ""
#| "[[Manually copy your persistent data to a new device|first_steps/"
#| "persistence/backup]]"
msgid ""
"[[Making a backup of your Persistent Storage|first_steps/persistence/backup]]"
msgstr ""
"[[کپی کردن دستی داده‌های مانا روی یک دستگاه جدید|first_steps/persistence/"
"copy]]"

#. type: Bullet: '  - '
#, fuzzy
#| msgid "[[Delete the persistent volume|first_steps/persistence/delete]]"
msgid ""
"[[Rescuing your Persistent Storage from a broken Tails|first_steps/"
"persistence/rescue]]"
msgstr "[[پاک کردن درایو مانا|first_steps/persistence/delete]]"

#. type: Bullet: '  - '
#, fuzzy
#| msgid "[[Delete the persistent volume|first_steps/persistence/delete]]"
msgid "[[Deleting the Persistent Storage|first_steps/persistence/delete]]"
msgstr "[[پاک کردن درایو مانا|first_steps/persistence/delete]]"

#~ msgid ""
#~ "How to use the persistent volume\n"
#~ "=================================\n"
#~ msgstr ""
#~ "چگونه از درایو مانا استفاده کنیم\n"
#~ "=================================\n"

#~ msgid ""
#~ "[[Change the passphrase of the persistent volume|first_steps/persistence/"
#~ "change_passphrase]]"
#~ msgstr ""
#~ "[[تغییر گذرواژهٔ درایو مانا|first_steps/persistence/change_passphrase]]"

#~ msgid ""
#~ "[[Check the file system of the persistent volume|first_steps/persistence/"
#~ "check_file_system]]"
#~ msgstr ""
#~ "[[بررسی نسخهٔ فایل درایو مانا|first_steps/persistence/check_file_system]]"

#~ msgid "<div class=\"note\">\n"
#~ msgstr "<div class=\"note\">\n"

#, fuzzy
#~| msgid ""
#~| "<p>It is only possible to create a persistent volume if the device, USB "
#~| "stick or\n"
#~| "SD card, was installed using <span class=\"application\">Tails "
#~| "Installer</span>.</p>\n"
#~ msgid ""
#~ "<p>It is only possible to create a persistent volume if the USB stick\n"
#~ "was installed using <span class=\"application\">Tails Installer</span>.</"
#~ "p>\n"
#~ msgstr ""
#~ "<p>تنها راه ممکن برای ایجاد درایوی مانا این است که \n"
#~ "دستگاه، یواس‌بی یا کارت حافظه‌ای که روی آن نصب می‌شود از <span class="
#~ "\"application\">نصب کنندهٔ تیلز</span> استفاده کند.</p>\n"

#, fuzzy
#~| msgid ""
#~| "<p>This requires a USB stick or SD card of <strong>at least 8 GB</"
#~| "strong>.</p>\n"
#~ msgid ""
#~ "<p>This requires a USB stick of <strong>at least 8 GB</strong>.</p>\n"
#~ msgstr ""
#~ "<p>برای این کار نیاز به یک درایو یواس‌بی یا کارت حافظه‌ای با <strong>حداقل "
#~ "۴ گیگابایت فضا</strong> دارید.</p>\n"

#~ msgid "</div>\n"
#~ msgstr "</div>\n"

#~ msgid "your personal files and working documents"
#~ msgstr "فایل‌های شخصی شما و مستنداتی که روی آن‌ها کار می‌کنید"

#~ msgid "the software packages that you download and install in Tails"
#~ msgstr "بسته‌های نرم‌افزاری که دانلود و روی تیلز نصب می‌کنید"

#~ msgid "the configuration of the programs you use"
#~ msgstr "تنظیمات برنامه‌هایی که استفاده می‌کنید"
